// This file is part of openZmeter.
//
// openZmeter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// openZmeter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with openZmeter.  If not, see <https://www.gnu.org/licenses/>
// ---------------------------------------------------------------------------------------------------------------------
#pragma once
#include "cctz/time_zone.h"
#include "HTTPServer.h"
#include "Rate.h"
#include "Database.h"
// ---------------------------------------------------------------------------------------------------------------------
using namespace cctz;
// ---------------------------------------------------------------------------------------------------------------------
class AnalyzerRates {
  private :
    const string              pSerial;
  private :
    time_zone                 pTimeZone;
    vector<vector<Rate> >     pRates;
    pthread_rwlock_t          pLock;
  public :
    static json CheckConfig(const json &config, const bool except);
  public :
    AnalyzerRates(const string &serial);
    virtual ~AnalyzerRates();
    void Config(const string &timezone, const json &rates);
    void GetCosts(HTTPRequest &req);
    void GetBills(HTTPRequest &req);
};
// ---------------------------------------------------------------------------------------------------------------------